using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Data
{
    public interface IDataContext
    {
        DbSet<Product> Products {get; set;}
        Task<int> SaveChangesAsync(CancellationToken cabcellationToken = default);
    }
}